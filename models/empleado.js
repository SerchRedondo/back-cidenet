const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;

const tipo_identificacion_validation = {
    values: ['CEDULA DE CIUDADANIA', 'CEDULA DE EXTRANJERIA', 'PASAPORTE', 'PERMISO ESPECIAL'],
    message: '{VALUE} no es un tipo valido'
}
const pais_validation = {
    values: ['COLOMBIA', 'ESTADOS UNIDOS'],
    message: '{VALUE} no es un pais valido'
}
const area_validation = {
    values: ['ADMINISTRACION', 'FINANCIERA', 'COMPRAS', 'INFRAESTRUCTURA', 'OPERACION', 'TALENTO HUMANO', 'SERVICIOS VARIOS'],
    message: '{VALUE} no es una area validad'
}


let empleadoSchema = new Schema({

    p_apellido: { type: String, required: [true, "el primer apellido no es valido"] },
    s_apellido: { type: String, required: [true, "el segundo apellido es requerido"] },
    p_nombre: { type: String, required: [true, "el primer nombre es requerido"] },
    s_nombre: { type: String, required: [true, "el segundo nombre es requerido"] },
    pais: { type: String, required: [true, "el pais es requerido"], enum: pais_validation },
    t_identificacion: { type: String, required: [true, "el tipo de indentificacion es requerido"], enum: tipo_identificacion_validation },
    n_identificacion: { type: Number, required: [true, "el numero de identificacion es requerido"], },
    correo: { type: String, unique: true, required: [true, "el correo es requerido"], maxlength: [300, 'El correo es muy largo'] },
    area: { type: String, required: [true, "el area es requerida"], enum: area_validation },
    estado: { type: String, default: "ACTIVO" },
    fecha_ingreso: { type: Date }

}, {
    timestamps: true,
});


module.exports = mongoose.model('Empleado', empleadoSchema);



