const mongoose = require('mongoose');

const Schema = mongoose.Schema;


const AreaSchema = new Schema({

    codigo: { type: Number },
    nombre: { type: String },
});
module.exports = mongoose.model('areas', AreaSchema);

