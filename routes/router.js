

const express = require('express');
const router = express.Router();

//importamos controladores
const empleadoController = require('./../controller/empleado');
const areaController = require('../controller/area');

// routa prueba 
router.get("/", (req, res) => {
    res.send({ "message": "Hola mundo" });
});


// rutas de empleadosController
router.post('/empleado', empleadoController.saveEmpleado);
router.get("/empleado", empleadoController.getEmpleados);
router.get('/empleado/:id', empleadoController.getOneEmpleado);
router.delete('/empleado/:id', empleadoController.delete);
router.put('/empleado/:id', empleadoController.updateEmpleado);

//rutas areaController
router.get("/area", areaController.getAreas);
router.post("/area", areaController.saveArea);


module.exports = router;