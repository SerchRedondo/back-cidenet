const mongoose = require('mongoose');
const Empleado = require('../models/empleado');


const dominio_col = "@cidenet.com.co";
const dominio_us = "@cidenet.com.us";


// controlador del modelo empleado el cual se encarga 
// de contener todos los metodos  para poder realizar las servicios Rest
const empleadoController = {

    //funcion para guardar un nuevo empleado
    saveEmpleado: async (req, res) => {

        console.log("INICIO  DEL SERVICIO POST");

        //obtemos los datos
        let body = req.body;


        //validamos que de no exista un empleado con el mismo tipo de cedula
        let cedula = await Empleado.findOne({ n_identificacion: parseInt(body.n_identificacion) });

        if (cedula) {
            //retornamos un error si el empleado si existe
            res.status(401).json({ ok: false, message: "El usuario ya existe en la base de datos" });
        } else {
            //procemos a crear el nuevo empleado
            //procemos a crear el nuevo correo para el usuario
            let correo = await crearEmail(body.p_nombre.trim().toLowerCase(), body.p_apellido.trim().toLowerCase().replace(/ /g, ""), body.pais);


            if (correo) {
                // creamos la instacion del nuevo empleado y asignamos los valores que sera guardados en la base de datos
                let empleado = new Empleado({
                    p_apellido: body.p_apellido.toUpperCase(),
                    s_apellido: body.s_apellido.toUpperCase(),
                    p_nombre: body.p_nombre.toUpperCase(),
                    s_nombre: body.s_nombre.toUpperCase(),
                    pais: body.pais,
                    correo: correo,
                    t_identificacion: body.t_identificacion,
                    n_identificacion: body.n_identificacion,
                    fecha_ingreso: body.f_ingreso,
                    area: body.area
                });

                ///se procede a guardar los datos en la base de datos
                empleado.save((error, empleado) => {
                    console.log("FIN DEL SERVICIO POST")
                    if (error)
                        return res.status(401).json({ ok: false, message: error, error: "El correo existe" });
                    if (!empleado)
                        return res.status(500).json({ ok: false, message: "El empleado no puedo ser creado con exito" });
                    return res.status(200).json({ ok: true, empleado });
                });
            } else {
                res.json({ message: "no creo el email" })
            }
            ;
        }
    },

    //listamos todos los empleados en la base de datos de
    // se reciben datos por parametros como el desde para poder visualizar los empleados
    getEmpleados: (req, res) => {
        console.log("INCIO DEL SERVICIO OBTENER EMPLEADOS");
        console.log(req.query);
        let desde = req.query.desde || 0;
        Empleado.find({ estado: "ACTIVO" })
            .skip(desde)
            .limit(10)
            .exec((error, empleados) => {
                console.log("FIN DEL SERVICIO OBTENER EMPLEADOS")
                if (error)
                    return res.status(401).send({ ok: false, message: error, error: "El correo existe" });
                if (!empleados)
                    return res.status(500).json({ ok: false, message: "El empleado no puedo ser creado con exito" });
                return res.status(200).json({ ok: true, empleados: empleados });
            });
    },

    //obetemos un empleado 
    
    getOneEmpleado: async (req, res) => {
        console.log("INICIO DEL SERVICIO OBTENER UN EMPLEADO")
        //obetenmos el id por paramtro para poder verificar la existencia del empleado
        let id = req.params.id;
       // console.log(id);
        try {
            let empleado = await Empleado.findOne({ _id: id });
            console.log(empleado);
            console.log("FIN DEL SERVICIO OBTENER EMPLEADO")
            if (empleado) {
                return res.status(200).json({ ok: true, empleado })
            }
        } catch (error) {
            if (error) {
                return res.status(400).json({ ok: false, message: "No existe el empleado", error })
            }
        }
    },
    //funcion para actualizar empleado
    updateEmpleado: async (req, res) => {
        console.log("INICIO DEL SERVICIO ACTUALIZAR EMPLEADOS")
        let id = req.params.id;
        console.log(id);
        let body = req.body;

        //permite determinar el dominio del empleado asi poder realizar una actualizacion eficiente con el nuevo dominio
        let dominio = await body.pais === "COLOMBIA" ? dominio_col : dominio_us;

        try {
            //verificamos si el empleado existe
            let empleado = await Empleado.findById({ _id: id });

            //obtenemos como resultado el nuevo correo electronico con los datos actualizados
            let correo = await dividirCorreo(body.p_nombre.toLowerCase(), body.p_apellido.toLowerCase(), empleado.correo, dominio);
            //le asignamos el correo actualizado al elemento que se enviara para actualizar
            body.correo = await correo;


            //se envia y actualiza la informacion nueva del empleado
            Empleado.findByIdAndUpdate(id, body, { new: true }, (error, empleado) => {

                if (error) {
                    return res.status(401).json({ ok: false, message: error, error: error });
                }

                if (!empleado) {
                    return res.status(500).json({ ok: false, message: "El empleado no puedo ser actualizado con exito" });
                }
                console.log("FINAL DEL SERVICIO PARA ACTUALIZAR EMPLEADO")
                return res.status(200).json({ ok: true, empleado });
            });


        } catch (error) {
            console.log("No existe")
            return res.status(401).json({ ok: false, message: "No existe el empleado" });
        }

    },
    //funcion para eliminar empleado
    delete: (req, res) => {
        console.log("INCIO DEL SERVICIO ELIMINAR EMPLEADO");
        //recibimos una id como parametros para poder verificar el empleado y eliminarlos en este caso dejarlo como inactivo de
        let id = req.params.id;

        //eliminamos el empleado
        Empleado.findByIdAndUpdate(id, { estado: "NO ACTIVO" }, { new: true }, (error, empleado) => {

            if (error) return res.status(401).json({ ok: false, message: error, error: error });
            if (!empleado) return res.status(500).json({ ok: false, message: "El empleado no puedo ser eliminado con exito" });

            console.log("FINAL DEL SERVICIO ELIMINAR EMPLEADO")
            return res.status(200).json({ ok: true, empleado });


        })



    }
}



//esta funcion se encargara de validar el origin de la persona que esta creando el email
const crearEmail = async (nombre, apellido, pais) => {

    let email = false;

    if (pais === "COLOMBIA") {
        // esta funcion realizara la crecion del email verificando en la base de datos de que no existe un correo igual 
        let result = await crearCorreo(nombre, apellido, dominio_col);
        console.log(result);
        return result;
    }///
    if (pais === "ESTADOS UNIDOS") {
        console.log("entro en estados unidos")
        // esta funcion realizara la crecion del email verificando en la base de datos de que no existe un correo igual 
        result = await crearCorreo(nombre, apellido, dominio_us);
        return result;
    }
    return email;
}




//funcion para buscar el correo
const buscarCorreo = async (correo) => {

    try {
        return await Empleado.findOne({ correo: correo });
    } catch (error) {

        return false;
    }

}





//este metodo recibe tres parametros nombre , apellido y dominio que puede ser de colombia o estados unidos
// se encargara de validar de que exista el correo si no existe creara uno nuevo 
const crearCorreo = async (nombre, apellido, dominio) => {

    //armamos el correo nuevo a crear correo
    let correo_nuevo = `${nombre}.${apellido}${dominio}`;

    //verificamos la existencia del correo nuevo
    let resultado = await buscarCorreo(correo_nuevo);


    //si el correo existe es  pasaremos a crear el nuevo correo con su forma respectiva
    if (resultado) {
        if (resultado.correo) {


            // lo que realizamos aqui es dividir el correo entre el nombre y dominio 
            // para poder manipular el nombre y apellido con facilidad
            // ý asi poder agregarle un indice ya que el correo normal ya esta siendo ocupado ejemplo nombre.apellido.1@cidenet.com.co
            let result1;
            var result2 = "";
            var result3 = "";
            let correo_split = resultado.correo.split("@");
            let correo = correo_split[0];
            let existe = true;
            var repeticion = true;


            //armamos nuestro nuevo correo a crear para
            let result = `${correo}.${1}${dominio}`;

            //validamos la existencia del correo
            var respuesta = await buscarCorreo(result);

            //si el correo existe entraremos en ciclo do while para poder recorrer los correos existentes en la base de datos y crear el correspondiente

            if (respuesta) {
                if (respuesta.correo) {
                    result3 = respuesta.correo;
                    do {


                        if (repeticion) {


                            result1 = await buscarCorreo(result3);
                            let res = await result1.correo.split("@");
                            let res2 = await res[0].split(".");
                            let num = await 1 + parseInt(res2[2]);


                            let correo = `${nombre}.${apellido}.${num}${dominio}`;
                            result3 = correo;

                            repeticion = false;
                        }

                        result2 = await buscarCorreo(result3);

                        if (result2) {

                            repeticion = true;
                        } else {

                            return result3;
                        }
                    } while (existe === true);
                    return false;
                }
            }
            return result;
        }
    } else {
        return correo_nuevo;
    }
}



//divide el correo para actualizarlo
const dividirCorreo = async (nombre, apellido, correo, dominio) => {

    let correo_split = await correo.split('@');

    let titulo_correo = await correo_split[0].split(".");

    if (titulo_correo.length > 2) {

        titulo_correo[0] = nombre;
        titulo_correo[1] = apellido;

        let final = `${titulo_correo[0]}.${titulo_correo[1]}.${titulo_correo[2]}${dominio}`;
        return final;
    } else {

        titulo_correo[0] = nombre;
        titulo_correo[1] = apellido;

        let final = `${titulo_correo[0]}.${titulo_correo[1]}${dominio}`;
        return final;
    }
}


// exportamos el modulo 
module.exports = empleadoController;
