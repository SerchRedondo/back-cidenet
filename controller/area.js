const Area = require('../models/area');


const AreaController = {


    getAreas: (req, res) => {
        Area.find({}, (err, areas) => {
            if (err) return res.status(400).json({ ok: false, message: err });
            res.json({ areas });
        });
    },

    saveArea: (req, res) => {
        let area = new Area({
            codigo: req.body.codigo,
            nombre: req.body.nombre
        });

        area.save((err, area) => {
            if (err) return res.status(401).json({ ok: false, err: err });
            return res.json({ area: area });
        });
    }
}


module.exports = AreaController;