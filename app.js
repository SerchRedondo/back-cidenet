'use strict';

const cors = require('cors');
const bodyParser = require('body-parser');
const express = require('express');
const app = express();

//importamos las rutas
const router = require('./routes/router');

//middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extends: true }))


//configuracion y cabeceras cors
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

app.use("/api", router);


//exportamos el modulo de app 


module.exports = app;
