require('dotenv').config();
const mongoose = require('mongoose');

//importamos el modulo app;

const app = require('./app');

mongoose.connect(process.env.DB_URL, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
}, (err, res) => {
    if (err) throw err;
    console.log("Conexion base de datos exitosa")
});

app.listen(process.env.PORT, () => {
    console.log("Servidor corriendo en localhost:" + process.env.PORT);
    console.log("http://localhost:5000/api");
})



